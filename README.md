# Gitlab Enhancy

Do some stuff that I couldn't get working in the normal tools like `lab`.

## Demo

![wait-ci](images/wait-ci.gif)

## Installation

Download binaries from the [release page](https://gitlab.oit.duke.edu/devil-ops/gitlab-enhancy/-/releases)

You can also pull from the normal devil-ops locations...check out the
[installing-devil-ops-packages](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
for more info


## Setup

Either set the `GITLAB_TOKEN` env var to a personal token, or use `token` in the config file

Defaults to using `gitlab.oit.duke.edu` for the gitlab server, but can be
overriddin with `GITLAB_HOST` or `host` in the config file

## Features

### Run CI

Run the CI process on a the project in your current working dir. This will also
wait for the CI to complete before exiting

```bash
$ glaby run-ci
...
```

Run the CI process on a given project

```bash
$ glaby run-ci --project-id foo/bar
...
```

### Wait CI

Wait for a CI process to complete in.

```bash
$ glaby wait-ci [--project-id=PROJECT_ID]
...
```

## Tweaks

Some items such as the GITLAB_TOKEN can be specified in the config file `~/.gitlab-enhancy.yaml`. Example:

```yaml
token: my-gitlab-token
compress-display: true
```
