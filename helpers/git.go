package helpers

import (
	"strings"

	"github.com/pkg/errors"
)

func ExtractNamespace(url string) (string, error) {
	/*
		Borrowed heavily from:
		https://github.com/zaquestion/lab
		Code above is CC 1.0 licensed (Public Domain)
	*/
	parts := strings.Split(url, "//")

	if len(parts) == 1 {
		part := parts[0]
		parts = strings.Split(part, ":")
	} else if len(parts) == 2 {
		part := parts[1]
		parts = strings.SplitN(part, "/", 2)
	} else {
		return "", errors.Errorf("cannot parse remote: %s", url)
	}

	if len(parts) != 2 {
		return "", errors.Errorf("cannot parse remote: %s", url)
	}
	path := parts[1]
	path = strings.TrimSuffix(path, ".git")
	return path, nil
}
