package helpers_test

import (
	"testing"

	"gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/helpers"
)

func TestExtractNamespace(t *testing.T) {
	tests := []struct {
		URL               string
		ExpectedNamespace string
	}{
		{
			URL:               "git@gitlab.oit.duke.edu:oit-ssi-systems/gitlab-enhancy.git",
			ExpectedNamespace: "oit-ssi-systems/gitlab-enhancy",
		},
		{
			URL:               "https://github.com/xanzy/go-gitlab.git",
			ExpectedNamespace: "xanzy/go-gitlab",
		},
	}
	for _, test := range tests {
		gotNS, err := helpers.ExtractNamespace(test.URL)
		if err != nil {
			t.Error(err)
		}
		if test.ExpectedNamespace != gotNS {
			t.Errorf("Expected NS %v but got %v", test.ExpectedNamespace, gotNS)
		}

	}

}
