package cmd

import (
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/glaby"
)

var projectId string

// waitCiCmd represents the followCi command
var waitCiCmd = &cobra.Command{
	Use:     "wait-ci [COMMIT]",
	Short:   "Wait for CI process to complete for a given commit",
	Args:    cobra.MaximumNArgs(1),
	Aliases: []string{"w"},
	Long:    `Uses the last commit in your current branch by default.`,
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		var project *gitlab.Project
		var ref string
		if projectId == "" {
			project, ref, err = glab.GetProjectWithLocalPath(targetDir)
			CheckErr(err)
		} else {
			project, _, err = glab.GitLab.Projects.GetProject(projectId, nil)
			CheckErr(err)
		}

		// Figure out what we are looking for
		var commitHash string
		if len(args) > 0 {
			commitHash = args[0]
		}

		// err = glab.WaitJobsTUI(project.PathWithNamespace, commitHash, ref)
		err = glab.WaitJobsTUI(&glaby.WaitJobsTUIOpts{
			ProjectID:       project.PathWithNamespace,
			CommitHash:      commitHash,
			Ref:             ref,
			CompressDisplay: v.GetBool("compress-display"),
		})
		CheckErr(err)
	},
}

func init() {
	rootCmd.AddCommand(waitCiCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// followCiCmd.PersistentFlags().String("foo", "", "A help for foo")
	waitCiCmd.PersistentFlags().StringVar(&projectId, "project-id", "", "Project id to follow. Defaults to local directory project")
	waitCiCmd.PersistentFlags().String("ref", "", "Branch/ref to look at. Defaults to the default branch")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// followCiCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
