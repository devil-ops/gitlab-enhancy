/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/glaby"
)

// runCiCmd represents the runCi command
var runCiCmd = &cobra.Command{
	Use:   "run-ci",
	Short: "Run CI Pipeline",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		outputTelegraf, err := cmd.Flags().GetBool("telegraf-metrics")
		CheckErr(err)
		var project *gitlab.Project
		ref, err := cmd.Flags().GetString("ref")
		CheckErr(err)
		if projectId == "" {
			project, ref, err = glab.GetProjectWithLocalPath(targetDir)
			CheckErr(err)
		} else {
			project, _, err = glab.GitLab.Projects.GetProject(projectId, nil)
			CheckErr(err)
		}

		// Figure out what we are looking for
		var commitHash string
		if len(args) > 0 {
			commitHash = args[0]
		}

		_, _, err = glab.GitLab.Pipelines.CreatePipeline(project.ID, &gitlab.CreatePipelineOptions{
			Ref: &ref,
		})
		CheckErr(err)

		// err = glab.WaitJobsTUI(project.PathWithNamespace, commitHash, ref)
		if !outputTelegraf {
			log.Debug().Str("project", project.PathWithNamespace).Str("commit", commitHash).Str("ref", ref).Bool("compress-display", v.GetBool("compress-display")).Msg("Wait options")
			err = glab.WaitJobsTUI(&glaby.WaitJobsTUIOpts{
				ProjectID:       project.PathWithNamespace,
				CommitHash:      commitHash,
				Ref:             ref,
				CompressDisplay: v.GetBool("compress-display"),
			})
			CheckErr(err)
		} else {
			err := glab.WaitCI(project.PathWithNamespace, "", ref)
			CheckErr(err)
			pi, err := glab.GetPipelineWithHash(project.PathWithNamespace, ref)
			CheckErr(err)
			jobs, err := glab.GetJobsWithPipeline(pi)
			CheckErr(err)
			if len(jobs) == 0 {
				log.Fatal().Msg("no jobs found in that pipeline")
			}

			for _, j := range jobs {
				for _, tag := range j.TagList {
					fmt.Printf("gitlab_ci_metric,status=%v,tag=%v,gitlab_url=%v latency_ms=%v %v\n", j.Status, tag, glab.GitLab.BaseURL().Host, j.FinishedAt.Sub(*pi.CreatedAt).Milliseconds(), time.Now().Unix())
				}
			}

		}
	},
}

func init() {
	rootCmd.AddCommand(runCiCmd)

	// Here you will define your flags and configuration settings.
	runCiCmd.PersistentFlags().StringVar(&projectId, "project-id", "", "Project id to follow. Defaults to local directory project")
	runCiCmd.PersistentFlags().String("ref", "main", "Branch or ref to start the pipeline on")

	runCiCmd.PersistentFlags().Bool("telegraf-metrics", false, "Include output for telegraf metrics")

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCiCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCiCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
