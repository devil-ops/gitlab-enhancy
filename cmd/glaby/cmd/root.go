package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/glaby"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile         string
	Verbose         bool
	gitlabProjectID int
	version         string = "dev"
	targetDir       string
	glab            *glaby.Glaby
	allowAdmin      bool
	compressDisplay bool
	v               *viper.Viper
	runStats        struct {
		Started time.Time
	}
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "gitlab-enhancy",
	Short:   "Do some additional things that I can't find another tool for",
	Long:    `Do some cool stuff with Gitlab that doesn't seem to fit in other packages`,
	Version: version,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		runStats = struct{ Started time.Time }{
			Started: time.Now(),
		}
		var err error
		glab, err = glaby.NewWithViper(v)
		CheckErr(err)

		glab.MustGreet()

		// Not super comfortable running this as a global admin in gitlab, please use a normal account
		CheckErr(err)
		if glab.IsAdmin() && !allowAdmin {
			log.Fatal().Msg(`Please use a non-admin account for this, admin accounts have too much access for
just looking at your own stuff. Duke thanks you for being careful ❤️! If you are
sure you are good, use --allow-admin to re-run with admin access allowed`)
		}
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		log.Info().Str("runtime", fmt.Sprint(time.Since(runStats.Started))).Msg("Completed")
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	v = viper.New()
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gitlab-enhancy.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Verbose logging")
	rootCmd.PersistentFlags().BoolVar(&allowAdmin, "allow-admin", false, "Allow admin access")
	rootCmd.PersistentFlags().BoolVar(&compressDisplay, "compress-display", false, `Compress the display output where possible. Note this may cause some
glitches in displaying lines with emojis, as they don't all fit quite
right unless there is spacing around the lines`)
	// err := viper.BindPFlag("compress-display", rootCmd.PersistentFlags().Lookup("compress-display"))
	rootCmd.PersistentFlags().StringVarP(&targetDir, "git-directory", "g", ".", "Git project directory")
	err := v.BindPFlag("compress-display", rootCmd.PersistentFlags().Lookup("compress-display"))
	cobra.CheckErr(err)

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		v.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cli" (without extension).
		v.AddConfigPath(home)
		v.SetConfigName(".gitlab-enhancy")
	}

	v.AutomaticEnv() // read in environment variables that match
	v.SetEnvPrefix("gitlab")

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if Verbose {
		log.Logger = log.Logger.With().Caller().Logger()
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// If a config file is found, read it in.
	if err := v.ReadInConfig(); err == nil {
		log.Debug().Str("file", v.ConfigFileUsed()).Msg("Using config file")
	}
}

func CheckErr(err error) {
	if err != nil {
		log.Fatal().Err(err).Msg("Error!")
	}
}
