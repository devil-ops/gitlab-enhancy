/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"math"
	"sort"

	"github.com/dustin/go-humanize"
	"github.com/rs/zerolog/log"
	"github.com/schollz/progressbar/v3"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

var (
	topN   int
	search string
	mine   bool
)

// sizeReportCmd represents the registry command
var sizeReportCmd = &cobra.Command{
	Use:   "size-report",
	Short: "Summarize Registry sizes",
	Run: func(cmd *cobra.Command, args []string) {
		type statSize struct {
			Bytes int64  `yaml:"bytes"`
			Human string `yaml:"human_readable"`
		}
		type stat struct {
			Name        string   `yaml:"name"`
			URL         string   `yaml:"url"`
			Total       statSize `yaml:"total"`
			LFS         statSize `yaml:"lfs"`
			Repository  statSize `yaml:"repository"`
			Registry    statSize `yaml:"registry"`
			JobArtifact statSize `yaml:"job_artifact"`
			// JobArtifactSizeHR string `yaml:"job_artifact_size_human_readable"`
		}
		c := make(chan stat)
		t := true
		opt := &gitlab.ListProjectsOptions{
			Statistics: &t,
			ListOptions: gitlab.ListOptions{
				PerPage: 100,
			},
		}
		if search != "" {
			opt.Search = &search
		}
		if mine {
			opt.Owned = &mine
		}
		bar := progressbar.Default(100)
		log.Info().Msg("Collecting project statistics from all of your projects, this will take a lil bit")
		var totalProjects int
		for {
			projs, resp, err := glab.GitLab.Projects.ListProjects(opt)
			bar.ChangeMax(resp.TotalItems)
			CheckErr(err)
			totalProjects = resp.TotalItems
			for _, proj := range projs {
				go func(proj *gitlab.Project) {
					bar.Add(1)
					s := stat{
						Name: proj.PathWithNamespace,
						URL:  proj.WebURL,
					}
					if proj.Statistics != nil {
						s.LFS = statSize{
							Bytes: proj.Statistics.LFSObjectsSize,
							Human: humanize.Bytes(uint64(proj.Statistics.LFSObjectsSize)),
						}
						s.Repository = statSize{
							Bytes: proj.Statistics.RepositorySize,
							Human: humanize.Bytes(uint64(proj.Statistics.RepositorySize)),
						}
						s.JobArtifact = statSize{
							Bytes: proj.Statistics.JobArtifactsSize,
							Human: humanize.Bytes(uint64(proj.Statistics.JobArtifactsSize)),
						}
					}
					regSize := 0
					registries, _, err := glab.GitLab.ContainerRegistry.ListProjectRegistryRepositories(proj.ID, nil)
					if err == nil {
						for _, registry := range registries {
							tagOpts := &gitlab.ListRegistryRepositoryTagsOptions{
								PerPage: 100,
							}
							for {
								tags, tagResp, err := glab.GitLab.ContainerRegistry.ListRegistryRepositoryTags(proj.ID, registry.ID, tagOpts)
								CheckErr(err)
								log.Debug().Str("project", proj.PathWithNamespace).Str("registry", registry.Path).Int("num_tags", tagResp.TotalItems).Int("page", tagResp.CurrentPage).Int("total_pages", tagResp.TotalPages).Msg("Processing tags")
								for _, tag := range tags {
									tagDetails, _, err := glab.GitLab.ContainerRegistry.GetRegistryRepositoryTagDetail(proj.ID, registry.ID, tag.Name)
									if err != nil {
										log.Debug().Err(err).Str("project", proj.PathWithNamespace).Str("registry", registry.Name).Str("tag", tag.Name).Msg("Could not get tag details")
									} else {
										regSize += tagDetails.TotalSize
									}
								}
								if tagResp.CurrentPage >= tagResp.TotalPages {
									break
								}
								tagOpts.Page = tagResp.NextPage
							}
						}
					}
					s.Registry = statSize{
						int64(regSize),
						humanize.Bytes(uint64(regSize)),
					}
					s.Total.Bytes += s.JobArtifact.Bytes + s.LFS.Bytes + s.Repository.Bytes + int64(regSize)
					s.Total.Human = humanize.Bytes(uint64(s.Total.Bytes))
					c <- s
				}(proj)
			}
			// log.Infof("!! %v of %v", resp.CurrentPage, resp.TotalPages)
			// Exit the loop when we've seen all pages.
			if resp.CurrentPage >= resp.TotalPages {
				break
			}
			opt.Page = resp.NextPage
		}
		var stats []stat
		for i := 0; i < totalProjects; i++ {
			s := <-c
			stats = append(stats, s)
		}
		sort.Slice(stats, func(i, j int) bool {
			return stats[i].Total.Bytes > stats[j].Total.Bytes
		})
		if topN > 0 {
			max := math.Min(float64(len(stats)), float64(topN))
			stats = stats[:int(max)]
		}
		data, err := yaml.Marshal(stats)
		CheckErr(err)
		fmt.Println("---")
		fmt.Println(string(data))
	},
}

func init() {
	rootCmd.AddCommand(sizeReportCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// sizeReportCmd.PersistentFlags().String("foo", "", "A help for foo")
	sizeReportCmd.PersistentFlags().IntVar(&topN, "topn", 0, "Only return the top N projects")
	sizeReportCmd.PersistentFlags().StringVar(&search, "search", "", "Only return projects matching the search string")
	sizeReportCmd.PersistentFlags().BoolVar(&mine, "mine", false, "Only return projects that I own")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// sizeReportCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
