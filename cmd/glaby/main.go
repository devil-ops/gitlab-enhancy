package main

import "gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/cmd/glaby/cmd"

func main() {
	cmd.Execute()
}
