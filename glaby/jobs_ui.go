package glaby

import (
	"bytes"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/xanzy/go-gitlab"
)

type JobsModel struct {
	quitting        bool
	err             error
	projectId       string
	url             string
	compressDisplay bool
	started         time.Time
	glaby           *Glaby
	pipeline        *gitlab.PipelineInfo
	jobC            chan *gitlab.Job
	doneC           chan error
	// jobInfo         map[int]*gitlab.Job
	// We are gonna need an order id here
	jobInfo     map[string]jobHolder
	spinnerInfo map[string]spinner.Model
	silent      bool
}

type JobsModelOpts struct {
	ProjectID string
	URL       string
	// InitJobs        []*gitlab.Job
	Pipeline        *gitlab.PipelineInfo
	Glaby           *Glaby
	CompressDisplay bool
	Silent          bool
	sc              chan *gitlab.Job
	dc              chan error
}

type jobHolder struct {
	Order int
	Job   *gitlab.Job
}

// func NewJobsModel(pid string, sc chan string, dc chan error) JobsModel {
func NewJobsModel(opts *JobsModelOpts) JobsModel {
	jm := JobsModel{
		started:         time.Now(),
		url:             opts.URL,
		jobC:            opts.sc,
		glaby:           opts.Glaby,
		compressDisplay: opts.CompressDisplay,
		jobInfo:         map[string]jobHolder{},
		spinnerInfo:     map[string]spinner.Model{},
		pipeline:        opts.Pipeline,
		doneC:           opts.dc,
		// timer:     timer.NewWithInterval(time.Second*5, time.Millisecond),
		projectId: opts.ProjectID,
	}
	jobs, err := opts.Glaby.getJobsWithPipeline(opts.Pipeline)
	if err != nil {
		panic(err)
	}

	jm.silent = opts.Silent

	for i, j := range jobs {
		jm.jobInfo[j.Name] = jobHolder{
			Order: i,
			Job:   j,
		}
		jm.spinnerInfo[j.Name] = newSpinner()
	}
	return jm
}

func newSpinner() spinner.Model {
	s := spinner.New()
	s.Spinner = spinner.Points
	s.Style = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	return s
}

func (m JobsModel) Init() tea.Cmd {
	inits := []tea.Cmd{}
	for k := range m.jobInfo {
		inits = append(inits, m.spinnerInfo[k].Tick)
	}
	return tea.Batch(inits...)
}

func (m JobsModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// Bizness logic
	select {
	case job := <-m.jobC:
		if _, ok := m.jobInfo[job.Name]; ok {
			// log.Warn().Int("job-id", job.ID).Msg("Existing spinner yoand spinner")
			i := m.jobInfo[job.Name].Order
			m.jobInfo[job.Name] = jobHolder{
				Order: i,
				Job:   job,
			}

		}
	case <-m.doneC:
		return m, tea.Quit
	}

	switch msg := msg.(type) {

	case tea.KeyMsg:
		switch msg.String() {
		case "q", "esc", "ctrl+c":
			m.quitting = true
			return m, tea.Quit
		default:
			return m, nil
		}
	case errMsg:
		// m.err = msg
		for k := range m.jobInfo {
			m.spinnerInfo[k].Update(tea.Quit)
		}
		return m, nil

	default:
		var cmds []tea.Cmd
		// m.spinner, cmd = m.spinner.Update(msg)
		for k := range m.jobInfo {
			var cmd tea.Cmd
			m.spinnerInfo[k], cmd = m.spinnerInfo[k].Update(msg)
			cmds = append(cmds, cmd)
		}
		return m, tea.Batch(cmds...)
	}
}

func (m JobsModel) View() string {
	if m.err != nil {
		return m.err.Error()
	}

	buffer := bytes.Buffer{}
	// Create an ordered slice of the jobs
	// TODO: This should be wrapped up in a func I think
	orderedJobs := []jobHolder{}
	for _, v := range m.jobInfo {
		orderedJobs = append(orderedJobs, v)
	}
	sort.Slice(orderedJobs, func(i, j int) bool {
		return orderedJobs[i].Order > orderedJobs[j].Order
	})

	for _, k := range orderedJobs {
		if j, ok := m.jobInfo[k.Job.Name]; ok {
			var icon string
			switch j.Job.Status {
			case "running":
				icon = m.spinnerInfo[k.Job.Name].View()
			default:
				icon = GetStateIcon(j.Job.Status)
			}
			var rs func(string) string
			if m.compressDisplay {
				rs = compressedRowStyle.Render
			} else {
				rs = rowStyle.Render
			}
			buffer.WriteString(
				rs(fmt.Sprintf(`%v [%v] %v %v`,
					minorStyle.Copy().PaddingRight(1).Render(icon),
					highlightStyle.Copy().Width(10).Align(lipgloss.Left).Render(j.Job.Status),
					normalStyle.Copy().Width(30).Align(lipgloss.Left).Render(j.Job.Name),
					minorStyle.Copy().Width(60).Align(lipgloss.Right).Render(j.Job.Stage),
				),
				))
			buffer.WriteString("\n")
		}
	}

	str := fmt.Sprintf(`
%v %v
%v
%v

`,
		titleStyle.Copy().Width(80).Align(lipgloss.Left).Render(m.projectId),
		normalStyle.Copy().Width(20).Align(lipgloss.Right).Render(fmt.Sprint(time.Since(m.started))),
		strings.TrimSpace(buffer.String()),
		footerStyle.Copy().Align(lipgloss.Center).Width(100).Render(m.url),
	)

	if !m.silent {
		if m.quitting {
			return str + "\n"
		}
		return str
	} else {
		return ""
	}
}
