package glaby

import (
	"errors"
	"fmt"
	"io"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/go-git/go-git/v5"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gitlab.oit.duke.edu/oit-ssi-systems/gitlab-enhancy/helpers"
)

type Glaby struct {
	GitLab *gitlab.Client
	me     *gitlab.User
}

func (g *Glaby) Greet() error {
	var err error
	g.me, _, err = g.GitLab.Users.CurrentUser()
	if err != nil {
		return nil
	}
	log.Info().Str("you", g.me.Name).Msg("Welcome to gitlab-enhancy!")
	return nil
}

func (g *Glaby) MustGreet() {
	err := g.Greet()
	if err != nil {
		log.Warn().Msg("Could not grett")
		panic(err)
	}
}

func (g *Glaby) IsAdmin() bool {
	me, _, err := g.GitLab.Users.CurrentUser()
	if err != nil {
		log.Warn().Err(err).Msg("Error detecting user")
		panic("Error detecting isAdmin")
	}
	return me.IsAdmin
}

func (g *Glaby) GetLatestCommit(namespace, ref string) (string, error) {
	return g.getLatestCommit(namespace, ref)
}

func (g *Glaby) getLatestCommit(namespace, ref string) (string, error) {
	commits, _, err := g.GitLab.Commits.ListCommits(namespace, &gitlab.ListCommitsOptions{
		RefName: &ref,
	})
	if err != nil {
		return "", err
	}
	return commits[0].ID, nil
}

func (g *Glaby) traceWithIdHash(id, hash string) (io.Reader, error) {
	// Figure out trace
	commit, _, err := g.GitLab.Commits.GetCommit(id, hash)
	if err != nil {
		log.Warn().Err(err).Msg("Error looking up commit")
		return nil, err
	}
	jobs, _, err := g.GitLab.Jobs.ListPipelineJobs(id, commit.LastPipeline.ID, nil)
	if err != nil {
		log.Warn().Err(err).Msg("Error looking up jobs")
		return nil, err
	}
	trace, _, err := g.GitLab.Jobs.GetTraceFile(id, jobs[0].ID)
	if err != nil {
		log.Warn().Err(err).Msg("Error looking up trace")
		return nil, err
	}
	return trace, nil
}

func (g *Glaby) getCIStatus(projectId, commitHash string) (string, *time.Duration, error) {
	commit, _, err := g.GitLab.Commits.GetCommit(projectId, commitHash)
	if err != nil {
		return "", nil, err
	}
	if commit.LastPipeline == nil {
		return "", nil, errors.New("No pipelines found")
	}
	timing := commit.LastPipeline.UpdatedAt.Sub(*commit.LastPipeline.CreatedAt)

	return commit.LastPipeline.Status, &timing, nil
}

func isPipelineStatusDone(s string) bool {
	// if s == "success" {
	if s == "success" || s == "failed" {
		return true
	}
	return false
}

type (
	pipelinestatus string
	jobtrace       string
)

func (g *Glaby) streamWaitWithProjectIDandHash(id, hash string, c chan string, e chan error) {
	log.Info().Msg("About to stream")
	var status string
	var err error
	var timing *time.Duration
	lastPing := time.Time{}
	for {
		// Look up every once in a while
		if time.Since(lastPing) > time.Second*5 {
			status, timing, err = g.getCIStatus(id, hash)
			if err != nil {
				log.Warn().Err(err).Msg("Error getting ci status")
				e <- err
				break
			}

		}
		slog := log.With().Str("status", status).Logger()

		if isPipelineStatusDone(status) {
			slog.Info().Str("duration", fmt.Sprint(timing)).Msg("Completed, shutting down pipeline!")
			e <- nil
		} else {
			c <- status
		}
	}
	log.Info().Msg("Done streaming")
}

func (g *Glaby) latestCommitOrString(projectId, commitHash, ref string) (string, error) {
	var err error
	if commitHash == "" {
		commitHash, err = g.getLatestCommit(projectId, ref)
		if err != nil {
			return "", err
		}
	}
	return commitHash, nil
}

func (g *Glaby) URLWithIDandCommit(id, commit string) (string, error) {
	commitO, _, err := g.GitLab.Commits.GetCommit(id, commit)
	if err != nil {
		return "", err
	}
	if commitO.LastPipeline == nil {
		return "", errors.New("No pipelines found")
	}
	return commitO.LastPipeline.WebURL, nil
}

func (g *Glaby) GetPipelineWithHash(pid, hash string) (*gitlab.PipelineInfo, error) {
	return g.getPipelineWithHash(pid, hash)
}

func (g *Glaby) getPipelineWithHash(pid, hash string) (*gitlab.PipelineInfo, error) {
	commit, _, err := g.GitLab.Commits.GetCommit(pid, hash)
	if err != nil {
		return nil, err
	}
	if commit.LastPipeline == nil {
		return nil, errors.New("no pipelines")
	}
	return commit.LastPipeline, nil
}

func (g *Glaby) GetJobsWithProjectAndCommit(pid, commit string) ([]*gitlab.Job, error) {
	return g.getJobsWithProjectAndCommit(pid, commit)
}

func (g *Glaby) getJobsWithProjectAndCommit(pid, commit string) ([]*gitlab.Job, error) {
	pi, err := g.getPipelineWithHash(pid, commit)
	if err != nil {
		return nil, err
	}

	jobs, _, err := g.GitLab.Jobs.ListPipelineJobs(pid, pi.ID, &gitlab.ListJobsOptions{})
	if err != nil {
		return nil, err
	}
	return jobs, nil
}

func (g *Glaby) GetJobsWithPipeline(p *gitlab.PipelineInfo) ([]*gitlab.Job, error) {
	return g.getJobsWithPipeline(p)
}

func (g *Glaby) getJobsWithPipeline(p *gitlab.PipelineInfo) ([]*gitlab.Job, error) {
	jobs, _, err := g.GitLab.Jobs.ListPipelineJobs(p.ProjectID, p.ID, &gitlab.ListJobsOptions{})
	if err != nil {
		return nil, err
	}
	return jobs, nil
}

type WaitJobsTUIOpts struct {
	ProjectID       string
	CommitHash      string
	Ref             string
	CompressDisplay bool
}

func (g *Glaby) WaitJobsTUI(opts *WaitJobsTUIOpts) error {
	commitHash, err := g.latestCommitOrString(opts.ProjectID, opts.CommitHash, opts.Ref)
	if err != nil {
		return err
	}
	slog := log.With().Str("project-id", opts.ProjectID).Str("commit-hash", commitHash[0:8]).Logger()

	pipeline, err := g.getPipelineWithHash(opts.ProjectID, commitHash)
	if err != nil {
		slog.Warn().Msg("no pipeline with that hash found")
		return err
	}

	c := make(chan *gitlab.Job)
	e := make(chan error)
	go g.streamWaitPipeline(opts.ProjectID, pipeline, c, e)
	p := tea.NewProgram(NewJobsModel(&JobsModelOpts{
		ProjectID:       opts.ProjectID,
		Pipeline:        pipeline,
		Glaby:           g,
		URL:             pipeline.WebURL,
		CompressDisplay: opts.CompressDisplay,
		sc:              c,
		dc:              e,
	}), tea.WithAltScreen())
	if err := p.Start(); err != nil {
		fmt.Println(err)
		return err
	}

	// Finish up here, just log out all the final job statuses
	jobs, err := g.getJobsWithPipeline(pipeline)
	if err != nil {
		return err
	}
	if len(jobs) == 0 {
		return errors.New("no jobs found in that pipeline")
	}

	for _, j := range jobs {
		jlog := log.With().
			Str("status", j.Status).
			Str("name", j.Name).
			Str("stage", j.Stage).
			Str("state", "complete").Logger()
		if j.FinishedAt != nil {
			jlog.Info().
				Str("took", fmt.Sprint(j.FinishedAt.Sub(*j.StartedAt))).
				Msg(GetStateIcon(j.Status))
		} else {
			jlog.Info().Msg(GetStateIcon(j.Status))
		}
	}
	log.Info().Str("web-url", pipeline.WebURL).Str("pipeline-took", fmt.Sprint(pipeline.UpdatedAt.Sub(*pipeline.CreatedAt))).Msg("🥳")

	return nil
}

func (g *Glaby) streamWaitPipeline(pid string, pipeline *gitlab.PipelineInfo, statusC chan *gitlab.Job, doneC chan error) {
	log.Info().Msg("About to stream pipeline")
	type jobState struct {
		Job     *gitlab.Job
		Checked time.Time
	}
	jobStates := map[int]*jobState{}
	var jobsLastChecked *time.Time
	var err error
	var jobs []*gitlab.Job

	for loop := true; loop; {
		// Should we time gate this?
		if jobsLastChecked == nil || (time.Since(*jobsLastChecked) > time.Second*5) {
			jobs, err = g.getJobsWithPipeline(pipeline)
			if err != nil {
				panic(err)
			}
		}

		if len(jobs) == 0 {
			log.Warn().Msg("No jobs found in this pipeline")
			panic("NoJobsFound")
		}

		for _, job := range jobs {
			s := &jobState{
				Job:     job,
				Checked: time.Time{},
			}
			jobStates[job.ID] = s

		}

		// Look up every once in a while
		for _, job := range jobs {
			if time.Since(jobStates[job.ID].Checked) > time.Second*5 {
				// status, timing, err = g.getCIStatus(job.Project.NameWithNamespace, job.Commit.String())
				nJob, _, err := g.GitLab.Jobs.GetJob(pid, job.ID, nil)
				if err != nil {
					log.Warn().Err(err).Msg("Error getting ci status")
					doneC <- err
					loop = false
				}
				jobStates[job.ID] = &jobState{
					Job:     nJob,
					Checked: time.Now(),
				}
			}

			// Get state of the entire thing here
			status, _, err := g.getCIStatus(pid, job.Commit.ID)
			if err != nil {
				log.Warn().Err(err).Msg("Error getting status")
			}
			slog := log.With().Str("status", status).Logger()
			if isPipelineStatusDone(status) {
				slog.Debug().Msg("Pipeline has completed")
				doneC <- nil
			} else {
				// Finally, just pass in the status
				statusC <- jobStates[job.ID].Job
			}
			// lastPing = time.Now()
		}
	}
	log.Info().Msg("Done streaming")
}

func (g *Glaby) streamWaitJobs(pid string, jobs []*gitlab.Job, statusC chan *gitlab.Job, doneC chan error) {
	log.Info().Msg("About to stream")
	type jobState struct {
		Job     *gitlab.Job
		Checked time.Time
	}
	jobStates := map[int]*jobState{}

	for _, job := range jobs {
		s := &jobState{
			Job:     job,
			Checked: time.Time{},
		}
		jobStates[job.ID] = s

	}
	for loop := true; loop; {
		// Look up every once in a while
		for _, job := range jobs {
			if time.Since(jobStates[job.ID].Checked) > time.Second*5 {
				// status, timing, err = g.getCIStatus(job.Project.NameWithNamespace, job.Commit.String())
				nJob, _, err := g.GitLab.Jobs.GetJob(pid, job.ID, nil)
				if err != nil {
					log.Warn().Err(err).Msg("Error getting ci status")
					doneC <- err
					loop = false
				}
				jobStates[job.ID] = &jobState{
					Job:     nJob,
					Checked: time.Now(),
				}
			}

			// Get state of the entire thing here
			status, _, err := g.getCIStatus(pid, job.Commit.ID)
			if err != nil {
				log.Warn().Err(err).Msg("Error getting status")
			}
			slog := log.With().Str("status", status).Logger()
			if isPipelineStatusDone(status) {
				slog.Debug().Msg("Pipeline has completed")
				doneC <- nil
			} else {
				// Finally, just pass in the status
				statusC <- jobStates[job.ID].Job
			}
			// lastPing = time.Now()
		}
	}
	log.Info().Msg("Done streaming")
}

func (g *Glaby) WaitCI(projectId, commitHash, ref string) error {
	// Get the latest commit hash if non is given
	commitHash, err := g.latestCommitOrString(projectId, commitHash, ref)
	if err != nil {
		return err
	}
	slog := log.With().Str("project-id", projectId).Str("short-commit", commitHash[0:8]).Logger()

	c := make(chan string)
	e := make(chan error)
	go g.streamWaitWithProjectIDandHash(projectId, commitHash, c, e)
	for {
		select {
		case status := <-c:
			slog.Debug().Str("status", status).Msg("status")
		case err := <-e:
			if err == nil {
				return nil
			} else {
				return err
			}
		}
	}
}

func (g *Glaby) GetProjectWithLocalPath(p string) (*gitlab.Project, string, error) {
	id, ref, err := g.GetProjectIDWithLocalPath(p)
	if err != nil {
		return nil, "", err
	}
	project, _, err := g.GitLab.Projects.GetProject(id, nil)
	if err != nil {
		return nil, "", err
	}
	return project, ref, nil
}

func (g *Glaby) GetProjectIDWithLocalPath(p string) (string, string, error) {
	r, err := PlainOpenWithPath(p)
	if err != nil {
		return "", "", err
	}
	id, err := g.GetProjectIDWithRepository(r)
	if err != nil {
		return "", "", err
	}
	h, err := r.Head()
	if err != nil {
		return "", "", err
	}
	return id, h.Name().Short(), nil
}

func (g *Glaby) GetProjectIDWithRepository(r *git.Repository) (string, error) {
	remotes, err := r.Remotes()
	if err != nil {
		return "", err
	}
	remoteURLString := remotes[0].Config().URLs[0]

	return helpers.ExtractNamespace(remoteURLString)
}

func New(host, token string) (*Glaby, error) {
	gl, err := gitlab.NewClient(token, gitlab.WithBaseURL(fmt.Sprintf("https://%s/api/v4", host)))
	if err != nil {
		return nil, err
	}
	return &Glaby{
		GitLab: gl,
	}, nil
}

func NewWithViper(v *viper.Viper) (*Glaby, error) {
	host := v.GetString("host")
	if host == "" {
		host = "gitlab.oit.duke.edu"
	}

	token := v.GetString("token")
	if token == "" {
		return nil, errors.New("must provide the token in viper. The easiest way is to set it in GITLAB_TOKEN")
	}
	return New(host, token)
}

func PlainOpenWithPath(path string) (*git.Repository, error) {
	// Open up a git object
	gitRoot, err := DetectGitPath(path)
	if err != nil {
		return nil, err
	}
	log.Debug().Str("git-root", gitRoot).Msg("Detected git repo root")

	return git.PlainOpen(gitRoot)
}

func GetStateIcon(state string) string {
	states := map[string]string{
		"running": "🏃‍♀️",
		"success": "✅",
		"failed":  "❌",
		"pending": "🥚",
		"created": "🐣",
		"skipped": "⏭",
	}
	if icon, ok := states[state]; ok {
		return icon
	} else {
		return "🤔"
	}
}
