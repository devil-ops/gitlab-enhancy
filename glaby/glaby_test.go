package glaby

import (
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	g, err := New("foo", "bar.com")
	require.NoError(t, err)
	require.NotNil(t, g)
}

func TestNewWithViper(t *testing.T) {
	v := viper.New()
	v.Set("host", "example.com")
	v.Set("token", "fake-token")
	g, err := NewWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, g)
}

func TestNewWithViperMissingToken(t *testing.T) {
	v := viper.New()
	v.Set("host", "example.com")
	g, err := NewWithViper(v)
	require.Error(t, err)
	require.EqualError(t, err, "must provide the token in viper. The easiest way is to set it in GITLAB_TOKEN")
	require.Nil(t, g)
}
