package glaby

import (
	"fmt"
	"strings"
	"time"
	"unicode"
)

type FmtDuration time.Duration

func (d FmtDuration) Format(f fmt.State, c rune) {
	prec, _ := f.Precision()
	f.Write([]byte(fmtTime(time.Duration(d), prec)))
}

func fmtTime(in time.Duration, prec int) string {
	s := in.String()
	ix := strings.IndexRune(s, '.')
	if ix == -1 {
		return s
	}
	unit := len(s)
	for i, x := range s[:ix+1] {
		if !unicode.IsDigit(x) {
			unit = i + ix + 1
			break
		}
	}
	if prec == 0 {
		return s[:ix] + s[unit:]
	}
	if prec > len(s)-ix-(len(s)-unit)-1 {
		prec = len(s) - ix - (len(s) - unit) - 1
	}
	return s[:ix+prec+1] + s[unit:]
}
