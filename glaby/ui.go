package glaby

import (
	"github.com/charmbracelet/lipgloss"
)

const (
	hotPink  = lipgloss.Color("#FF06B7")
	darkGray = lipgloss.Color("#767676")
)

var (
	highlightStyle     = lipgloss.NewStyle().Foreground(hotPink)
	titleStyle         = lipgloss.NewStyle().Bold(true)
	normalStyle        = lipgloss.NewStyle().Foreground(darkGray)
	minorStyle         = lipgloss.NewStyle().Foreground(darkGray).Faint(true)
	rowStyle           = lipgloss.NewStyle().PaddingBottom(1)
	compressedRowStyle = lipgloss.NewStyle().PaddingBottom(0)
	footerStyle        = lipgloss.NewStyle().PaddingTop(1).Underline(true)
)

type errMsg error

/*
type CIModel struct {
	spinner   spinner.Model
	timer     timer.Model
	quitting  bool
	err       error
	projectId string
	status    string
	url       string
	stateC    chan string
	doneC     chan error
}

type CIModelOpts struct {
	ProjectID string
	URL       string
	sc        chan string
	dc        chan error
}

// func NewCIModel(pid string, sc chan string, dc chan error) CIModel {
func NewCIModel(opts *CIModelOpts) CIModel {
	s := spinner.New()
	s.Spinner = spinner.Dot
	s.Style = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	return CIModel{
		spinner: s,
		url:     opts.URL,
		stateC:  opts.sc,
		doneC:   opts.dc,

		timer:     timer.New(time.Second * 60),
		projectId: opts.ProjectID,
	}
}

func (m CIModel) Init() tea.Cmd {
	return tea.Batch(m.spinner.Tick, m.timer.Init())
}

func (m CIModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// Bizness logic
	select {
	case status := <-m.stateC:
		m.status = status
	case <-m.doneC:
		return m, tea.Quit
	}

	switch msg := msg.(type) {

	case tea.KeyMsg:
		switch msg.String() {
		case "q", "esc", "ctrl+c":
			m.quitting = true
			return m, tea.Quit
		default:
			return m, nil
		}

	case errMsg:
		// m.err = msg
		m.spinner.Update(tea.Quit)
		return m, nil
	case timer.TickMsg:
		var cmd tea.Cmd
		m.timer.Start()
		m.timer, cmd = m.timer.Update(msg)
		return m, cmd

	default:
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)
		return m, cmd
	}
}

func (m CIModel) View() string {
	if m.err != nil {
		return m.err.Error()
	}
	s := m.timer.View()
	str := fmt.Sprintf(`
%v [%v] %v (Running: %v) - %v

%v


`,
		m.spinner.View(),
		highlightStyle.Render(m.status),
		normalStyle.Width(30).Render(m.projectId),
		s,
		m.timer.Running(),
		normalStyle.Width(100).Render(m.url),
	)
	if m.quitting {
		return str + "\n"
	}
	return str
}
*/
